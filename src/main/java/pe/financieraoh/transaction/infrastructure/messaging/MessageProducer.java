package pe.financieraoh.transaction.infrastructure.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.financieraoh.transaction.infrastructure.config.SpringAmqpConfig;

@Component
public class MessageProducer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageProducer.class);

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public MessageProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(String message) {
    	LOGGER.info("IN Queue message financieraoh-transaction: {}", message);
        rabbitTemplate.convertAndSend(SpringAmqpConfig.EXCHANGENAME, SpringAmqpConfig.ROUTINGKEY, message);
    }
}