package pe.financieraoh.transaction.infrastructure.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MessageUtil {
		
	private static final MessageUtil _instance = new MessageUtil();
	private static int posMensaje;

	private MessageUtil() {		
	}
	
	public static MessageUtil getInstance() {
		posMensaje = 0;
		return _instance;
	}

	private static void setPosMensaje(int posMensaje) {
		MessageUtil.posMensaje = posMensaje;
	}

	public String getSubString(int cantCaracteres, String msn) {

		String strSubCadena = "";
		int posFinalMensaje = posMensaje + cantCaracteres;
		if (posMensaje < msn.length()) {
			if (posFinalMensaje > msn.length()) {
				strSubCadena = msn.substring(posMensaje, msn.length());
			} else {
				strSubCadena = msn.substring(posMensaje, posFinalMensaje);
			}
		} else {
			strSubCadena = "";
		}
		setPosMensaje(posMensaje + cantCaracteres);
		return strSubCadena;

	}

	public static String convertAlphanumericAString(Object obj, int size) {

		String strOb = "";

		if (obj != null) {
			strOb = String.valueOf(obj);
		}

		int resto = size - strOb.length();
		for (int i = 0; i < resto; i++) {
			strOb = strOb.concat(" ");
		}
		if (strOb.length() > size) {
			strOb = strOb.substring(0, size);
		}
		return strOb;
	}

	public static String convertNumericAString(Object obj, int size) {

		String strOb = "";

		if (obj != null) {
			strOb = String.valueOf(obj);
		}

		int resto = size - strOb.length();
		for (int i = 0; i < resto; i++) {
			strOb = "0".concat(strOb);
		}
		if (strOb.length() > size) {
			strOb = strOb.substring(0, size);
		}
		return strOb;
	}

	public static boolean isNumeric(String cadena) {
		try {
			cadena = cadena==null || "".equals(cadena)?"0":cadena;
			new BigDecimal(cadena.trim());
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	public static BigDecimal getFormatDecimal(String msn) {

		BigDecimal bd;
		msn = msn==null || "".equals(msn)?"0":msn;
		if (msn.length() >= 14 && msn.trim().length() > 12) {
			msn = msn.substring(1, 12).concat(".").concat(msn.substring(12));
		}
		
		bd = new BigDecimal(msn);
		bd = bd.setScale(2, RoundingMode.DOWN);
		return bd;
	}
}