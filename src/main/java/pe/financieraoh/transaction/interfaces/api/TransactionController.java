package pe.financieraoh.transaction.interfaces.api;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.financieraoh.transaction.application.service.TransactionService;
import pe.financieraoh.transaction.domain.model.etransac.Transaction;
import pe.financieraoh.transaction.infrastructure.exception.ModelNotFoundException;
import pe.financieraoh.transaction.infrastructure.messaging.MessageProducer;
import pe.financieraoh.transaction.infrastructure.util.DateUtil;
import pe.financieraoh.transaction.infrastructure.util.MessageRqConstant;
import pe.financieraoh.transaction.infrastructure.util.MessageUtil;
import pe.financieraoh.transaction.interfaces.dto.TransactionQueryRequest;
import pe.financieraoh.transaction.interfaces.dto.TransactionQueryResponse;
 
@RestController
@RequestMapping("/api/transactions")
@Api(value = "transactions")
public class TransactionController {
            private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);
 
            @Autowired
            private MessageProducer messageProducer;
 
            private final TransactionService transactionService;
 
            public TransactionController(TransactionService transactionService) {
                        this.transactionService = transactionService;
            }
 
            /**
            * Registra Transaction.
            *
            * @param message String
            * @return message String
            */
            @ApiOperation(value = "Registra una Transaccion", httpMethod = "POST", produces = "text/plain", consumes = "text/plain")
            @ApiResponses(value = {
                                    @ApiResponse(code = 200, message = "Transaccion creada"), @ApiResponse(code = 500, message = "No creado") })
            @PostMapping("/create")
            public ResponseEntity<String> create(
                                    @ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody String message) {
                        LOGGER.info("create Message Request: {}", message);
                        String response = "";
                        Transaction transaction = null;
                        try {
                        transaction =  buildCreateTransaction(message);
                        Long id = transactionService.create(transaction);
                        if (id != 0) {
                                    messageProducer.sendMessage(message);
                                    transaction.setId(id);
                                    response = buildResponse(transaction,true);
                        }
                        }
                        catch (Exception e){
                                    LOGGER.error("Error en create method: {}", e.getMessage());
                                    response = buildResponse(transaction,false);
                        }
                        
                        LOGGER.info("create Message Response: {}", message);
                        return new ResponseEntity<>(response, HttpStatus.OK);
            }           
            
            public Transaction buildCreateTransaction(String message)  {
                        Transaction transaction=null;
                        String messageReqType;
                        String uuid;
                        String bank;
                        String application;
                        String user;
                        String trace;
                        String charset;
                        String filler;
                        try {
                                    transaction = new Transaction();
                                    messageReqType = message.substring(0, 1);
                                    uuid = message.substring(1,17);
                                    bank = message.substring(17,20);
                                    application = message.substring(20,30);
                                    user = message.substring(30,50);
                                    trace = message.substring(50,56);
                                    charset = message.substring(56,57);
                                    filler = message.substring(57,77);
                                    LOGGER.info(
                                                           "Message header: messageReqType: {}, uuid: {}, bank: {}, application: {}, user: {}, trace: {}, charset: {}, filler: {}",
                                                           messageReqType, uuid, bank, application, user, trace, charset, filler);
                                    String messageType = message.substring(77,81);
                                    String cardNumber = message.substring(81,100);
                                    String transactionCode = message.substring(100,102);
                                    String accountType = message.substring(102,106);
                                    String reverse = message.substring(106,107);
                                    String amountLocal = message.substring(107,121);
                                    String amountOriginal = message.substring(121,135);
                                    String amountCollection = message.substring(135,149);
                                    String transactionDate = message.substring(149,163);
                                    String conversionRate = message.substring(163,174);
                                    String transactionNumber = message.substring(174,180);
                                    String transactionHour = message.substring(180,186);
                                    String transactionDateOnly = message.substring(186,194);
                                    LOGGER.info(
                                                           "Message body: messageType: {}, cardNumber: {}, transactionCode: {}, accountType: {}, reverse:{}, amountLocal: {}, amountOriginal: {}, amountCollection: {}, transactionDate: {}, conversionRate: {}, transactionNumber: {}, transactionHour: {}, transactionDateOnly: {}",
                                                           messageType, cardNumber, transactionCode, accountType, reverse, amountLocal, amountOriginal,
                                                           amountCollection, transactionDate, conversionRate, transactionNumber, transactionHour,
                                                           transactionDateOnly);
                                    transaction.setOperacion(trace);
                                    transaction.setTrace(trace);
                                    transaction.setData(message);
                                    transaction.setTrxDateTime(DateUtil.convertStringToDate(transactionDate.trim(), DateUtil.PATTERN_DATE_ISO));
                                    transaction.setTrxDate(DateUtil.convertStringToDate(transactionDateOnly.trim(), DateUtil.PATTERN_YYYYMMDD));
                                    transaction.setTrxHour(DateUtil.convertStringToDate(transactionHour.trim(), DateUtil.PATTERN_TIME_HHMMSS));
                                    transaction.setUuid(uuid.trim());
                                    transaction.setCreatedBy(user);
                        } catch (Exception e) {
                                    LOGGER.error("Error in Parse Input: {}", (e.getMessage() ));                                  
                        }
                        return transaction;
            }
 
            public String buildResponse(Transaction transaction, boolean success) {
                        StringBuilder data2 = new StringBuilder("");
                        data2.append(MessageUtil.convertAlphanumericAString(MessageRqConstant.MSG_RESPONSE_CODE,
                                               MessageRqConstant.MSG_TYPE_REQUEST));
                        data2.append(MessageUtil.convertAlphanumericAString(transaction.getUuid(), MessageRqConstant.MSG_UUID));
                        if(success) {
                        data2.append(MessageUtil.convertAlphanumericAString(MessageRqConstant.SUCCESS_RESPONSE,
                                               MessageRqConstant.MSG_RES_CODE));
                        }
                        else {
                                    data2.append(MessageUtil.convertAlphanumericAString(MessageRqConstant.FAIL_RESPONSE,
                                                           MessageRqConstant.MSG_RES_CODE));      
                                    
                        }
                        
                        data2.append(MessageUtil.convertNumericAString(transaction.getTrace(), MessageRqConstant.MSG_TRACE));
                        data2.append(MessageUtil.convertAlphanumericAString(transaction.getOperacion(), MessageRqConstant.MSG_FILLER));
                        return data2.toString();
            }
            
            
 
            @ApiOperation(value = "Registra una Transaccion Asincrona", httpMethod = "POST", consumes = "text/plain")
            @PostMapping("/createAsync")
            public ResponseEntity<Object> createAsync(
                                    @ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody String message) {
                        LOGGER.info("createAsync Message Request: {}", message);
                        Transaction transaction = null;
                        transaction =  buildCreateTransaction(message);
                        Long id = transactionService.create(transaction);
                        if (id != 0) {
                            messageProducer.sendMessage(message);
                        }
                        return new ResponseEntity<>(HttpStatus.OK);
            }
 
            @ApiOperation(value = "Validar una transaccion enviada")
            @ApiResponses(value = { //
                                    @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Errores") })
            @PostMapping(value = "/validate")
            public ResponseEntity<String> validate(
                                    @ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody String uuid) {
                        LOGGER.info("validate Message Request: {}", uuid);
                        String response = "NO";
                        Transaction transaction = null;
                        transaction = transactionService.findByUuid(uuid);
                        if (transaction != null) {
                                    response = "OK";
                        }
                        LOGGER.info("validate Message Response: {}", response);
                        return new ResponseEntity<>(response, HttpStatus.OK);
            }
 
            @ApiOperation(value = "Listado de Transacciones Enviadas", response = TransactionQueryResponse.class)
            @ApiResponses(value = { @ApiResponse(code = 200, message = "Resultados"),
                                    @ApiResponse(code = 404, message = "No hay registros devueltos") })
            @PostMapping(value = "/list", produces = "application/json")
            public ResponseEntity<List<TransactionQueryResponse>> list(
                                    @ApiParam(value = "Trama de Autorizacion Request", required = true) @RequestBody TransactionQueryRequest filter)
                        {
 
                        LOGGER.info("list Message Request: {}", filter);
 
                        Date transactionFromHour;
                        Date transactionToHour;
                        List<Transaction> listTransaction;
                        List<TransactionQueryResponse> listado = null;
 
                        transactionFromHour = DateUtil.convertStringToDate(
                                               filter.getTransactionDate().concat(filter.getTransactionFromHour()), DateUtil.PATTERN_DATE_ISO);
                        transactionToHour = DateUtil.convertStringToDate(
                                               filter.getTransactionDate().concat(filter.getTransactionToHour()), DateUtil.PATTERN_DATE_ISO);
                        listTransaction = transactionService.findByRangesHours(transactionFromHour, transactionToHour);
                        listado = new ArrayList<>();
                        for (Transaction transaction : listTransaction) {
                                    TransactionQueryResponse query = new TransactionQueryResponse();
                                    query.setMessageType(MessageRqConstant.MSG_RESPONSE_CODE);
                                    query.setUuid(transaction.getUuid());
                                    query.setTransactionDate(DateUtil.convertDateToStringYYYYMMDDHHMMSS(transaction.getTrxDateTime()));
                                    query.setResponseCode(MessageRqConstant.SUCCESS_RESPONSE);
                                    listado.add(query);
                        }                       
                        if (!listado.isEmpty()) {
                                    LOGGER.info("list Message Response: {}", listado);
                                    return ResponseEntity.ok().body(listado);
                        }else{
                                    LOGGER.info("list Message Response: {}", "No hay registros devueltos");
                                    throw new ModelNotFoundException("No hay registros devueltos");
                        }
            }
 
}