package pe.financieraoh.transaction.application.service;

import java.io.Serializable;

/**
 * Defines all public business behaviors for operations on the entity model
 *
 * @param <T>  the entity
 * @param <ID> the primary id
 * @author Xinh Nguyen
 */
public interface BaseService<T, V extends Serializable> {

    /**
     * Create a new entity
     *
     * @param t the entity should be created
     * @return a primary key
     */
    V create(T t);

}